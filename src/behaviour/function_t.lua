local Class = require "vendor/hump.class"
local Behaviour = require "behaviour/behaviour"

local FunctionT = Class {
    __includes = { Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function FunctionT:setup(fn)
    self.fn = fn
end

function FunctionT:update(dt, entity)
    self.fn(self, dt, entity)
end

function FunctionT:reset(entity)
    Behaviour.reset(self, entity)
end

return FunctionT