local Class = require "vendor/hump.class"
local EventEmitter = require "behaviour/eventemitter"
local Resettable = require "behaviour/resettable"

local AbstractBehaviour = Class {
    __includes = { EventEmitter, Resettable },
    init = function(self, ...)
        Resettable.init(self, ...)
        EventEmitter.init(self)
        self:setup(...)
    end
}

function AbstractBehaviour:reset(entity)
    Resettable.reset(self)
    EventEmitter.emit(self, 'reset', entity)
end

return AbstractBehaviour