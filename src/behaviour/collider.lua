local Class = require "vendor/hump.class"
local CollisionAware = require "behaviour/collisionaware"
local Behaviour = require "behaviour/behaviour"

local Collider = Class {
    __includes = { CollisionAware, Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function Collider:setup(collisionFilter, collisionResolver, targetExtractor)
    self.filter = collisionFilter
    self.resolver = collisionResolver
    self.target = targetExtractor or (function(entity) return entity end)
end

function Collider:update(dt, entity)
    local target = self.target(entity)
    local rect = target.world:getRect(target)
    CollisionAware.move(self, target, rect.l, rect.t, 0, function(other) return self.filter(target, other) end, self.resolver)
end

return Collider