local Class = require "vendor/hump.class"
local Signal = require "vendor/hump.signal"

local EventEmitter = Class {
    init = function(self)
        self.signals = Signal.new()
    end
}

function EventEmitter:emit(eventName, ...)
    self.signals:emit(eventName, ...)
end

function EventEmitter:on(name, delegate)
    self.signals:register(name, delegate)
end

return EventEmitter
