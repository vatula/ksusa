local Class = require "vendor/hump.class"
local Behaviour = require "behaviour/behaviour"
local CollisionAware = require "behaviour/collisionaware"

local function collisionFilter(other)
    local tags = other.tags
    return tags ~= nil and (tags['ground'] or tags['blocker'])
end

local MotionContinuous = Class {
    __includes = { CollisionAware, Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function MotionContinuous:setup(axis, velocity)
    self.axis = axis
    self.velocity = velocity
end

function MotionContinuous:update(dt, entity)
    local rect = entity:getRect()
    local future_l, future_t = rect.l, rect.t
    if self.axis == 'y' then future_t = future_t + self.velocity*dt end
    if self.axis == 'x' then future_l = future_l + entity.direction*self.velocity*dt end

    CollisionAware.move(self, entity, future_l, future_t, 0, collisionFilter, function(tl, tt, nx, ny, sl, st, alpha)
        if nx ~= 0 then
            entity:flip() -- turn around
            return true -- collision resolved
        else
            entity:move(tl, tt, alpha)
            return false
        end
    end)

end

return MotionContinuous