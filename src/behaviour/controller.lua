local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"
local CollisionAware = require "behaviour/collisionaware"
local Behaviour = require "behaviour/behaviour"
local vector = require "vendor/hump.vector"

local function collisionFilter(other)
    local tags = other.tags
    return tags ~= nil and tags['ground']
end

local Controller = Class {
    __includes = { CollisionAware, Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function Controller:setup(velocity)
    self.velocity = velocity
end

function Controller:reset(entity)
    Behaviour.reset(self, entity)
end

function Controller:update(dt, entity)
    local rect = entity:getRect()
    local future_l, future_t = rect.l, rect.t
    if love.keyboard.isDown('left') then
        future_l = future_l - self.velocity*dt
    elseif love.keyboard.isDown('right') then
        future_l = future_l + self.velocity*dt
    else
        self:emit('controller.idle', entity)
    end

    if ((rect.l - future_l > 0) and (entity.direction > 0)) or ((rect.l - future_l < 0) and (entity.direction < 0)) then
        entity:flip()
    end

    CollisionAware.move(self, entity, future_l, future_t, 0, collisionFilter, function(tl, tt, nx, ny, sl, st, alpha)
        entity:move(tl, tt, alpha)
        return true
    end)
end

return Controller