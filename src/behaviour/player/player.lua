local Timer = require "vendor/hump.timer"
local Behaviours = require "entities.behaviours"
local FunctionT = require "behaviour.function_t"
local BehaviorGroup = require "behaviour.behaviorgroup"
local Gravity = require "behaviour.gravity"
local Controller = require "behaviour.controller"
local Collider = require "behaviour.collider"
local Signal = require "vendor/hump.signal"

local function switch(state)
    return function(entity)
        entity.signals:emit('stateChange', state, true)
    end
end

local function player()
    local function onGround(entity)
        entity.__hits = nil
        if entity.__state and entity.__state == 'jump' then
            entity.__state = nil
            entity.__jumps = nil
            entity.signals:emit('stateChange', 'idle', true)
        end
    end

    local behaviours = Behaviours()

    local gravity = Gravity(10)
    local idle = FunctionT(function(self, dt, entity)
        if (love.keyboard.isDown('left') or love.keyboard.isDown('right')) and entity.__gravityOnGround then
            entity.signals:emit('stateChange', 'walk', true)
        end
    end)
    gravity:on('onGround', onGround)
    local walkController = Controller(50)
    walkController:on('controller.idle', function(entity)
        if entity.__gravityOnGround then
            entity.signals:emit('stateChange', 'idle', true)
        end
    end)
    local jumpController = Controller(50)
    local jump = Gravity(10, -250)
    jump:on('onGround', onGround)
    jump:on('reset', function(entity)
        entity.__state = 'jump'
        entity.__jumps = (entity.__jumps or 0) + 1
    end)
    local adversarialCollider = Collider(function(entity, other)
        local tags = other.tags
        return tags ~= nil and tags['enemy'] and not entity.__invincible
    end, function(tl, tt, nx, ny, sl, st, alpha, col)
        col.item.__invincible = true
        Timer.add(0.5, function() col.item.__invincible = false end)
        col.item.signals:emit('damaged', 20)
        return true
    end)

    local consumablesCollider = Collider(function(entity, other)
        local tags = other.tags
        return tags ~= nil and tags['consumable'] and not entity.__invincible
    end, function(tl, tt, nx, ny, sl, st, alpha, col)
        col.other.consumed = true
        local old = col.item.consumed[col.other.name] or 0
        col.item.consumed[col.other.name] = old + 1
    end)

    Signal.clear('player.jump')
    Signal.register('player.jump', function(entity)
        if (entity.__jumps and entity.__jumps < 2) or not entity.__jumps then
            entity.signals:emit('stateChange', 'jump', true)
        end
    end)

    -- attack

    Signal.clear('player.hit')
    Signal.register('player.hit', function(entity)
        if not entity.__invincible then
            if not entity.__gravityOnGround and entity.__hits and entity.__hits > 0 then
                return
            end
            entity.__hits = (entity.__hits or 0) + 1
            entity:hit()
        end
    end)

    behaviours:add('idle', BehaviorGroup(gravity, idle, adversarialCollider, consumablesCollider))
    behaviours:add('walk', BehaviorGroup(gravity, walkController, adversarialCollider, consumablesCollider))
    behaviours:add('jump', BehaviorGroup(jump, jumpController, adversarialCollider, consumablesCollider))
    return behaviours
end

return player