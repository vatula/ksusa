local Timer = require "vendor/hump.timer"
local Behaviours = require "entities.behaviours"
local Patrol = require "behaviour.patrol"
local TimedMotion = require "behaviour.motion_timed"
local FunctionT = require "behaviour.function_t"
local BehaviorGroup = require "behaviour.behaviorgroup"
local Creature = require "entities.creature.creature"
local Animation = require "animation/animation"
local Gravity = require "behaviour.gravity"
local garbageAnimation = require "configuration.animations.garbage"

local function switch(state)
    return function(entity)
        entity.signals:emit('stateChange', state, true)
    end
end

local function garbageBehaviour()
    local behaviours = Behaviours()
    local gravity = Gravity(2)
    gravity:on('onGround', switch('explode'))
    local idle = FunctionT(function(self, dt, garbage)
        local parentRect = garbage.__parent:getRect()
        local rect = garbage:getRect()
        garbage:move(parentRect.l, parentRect.t)
    end)
    local explode = FunctionT(function() end)
    explode:on('reset', function(garbage)
        Timer.add(0.24, function() garbage:acceptDamage(garbage.energy) end)
    end)
    behaviours:add('idle', BehaviorGroup(idle))
    behaviours:add('attached', BehaviorGroup(idle))
    behaviours:add('fall', BehaviorGroup(gravity))
    behaviours:add('explode', BehaviorGroup(explode))

    return behaviours
end

local function garbage(world, rect)
    local item = Creature(world,
    {
        rect = {l = rect.l, t = rect.t, w = 16, h = 16}, energy = 10,
        animation = Animation(garbageAnimation.layers), behaviours = garbageBehaviour()
    })
    item.tags = {['enemy'] = true }
    return item
end

local function ibis()
    local behaviours = Behaviours()

    local idle = TimedMotion('y', 'linear', 2, 0)
    local flap1 = TimedMotion('y', 'in-out-back', 0.4, -10)
    local flap2 = TimedMotion('y', 'in-out-back', 0.6, -20)
    local patrol = TimedMotion('x', 'linear', 10, -300)

    idle:on('finished', switch('flap1'))
    flap1:on('reset', function(entity)
        entity.__garbage = garbage(entity.world, entity:getRect())
        entity:attach('garbage', entity.__garbage)
    end)
    flap1:on('finished', switch('flap2'))
    flap2:on('reset', function(entity)
        entity.__flaps = entity.__flaps or 0
        entity.animation:rewind('flap2')
    end)
    flap2:on('finished', function(entity)
        if entity.__flaps == 3 then
            entity.__flaps = nil
            switch('patrol')(entity)
        else
            entity.__flaps = entity.__flaps + 1
            switch('flap2')(entity)
        end
    end)
    patrol:on('reset', function(entity)
        entity.__garbage.signals:emit('stateChange', 'fall', true)
    end)

    behaviours:add('idle', BehaviorGroup(idle))
    behaviours:add('flap1', BehaviorGroup(flap1))
    behaviours:add('flap2', BehaviorGroup(flap2))
    behaviours:add('patrol', BehaviorGroup(patrol))
    return behaviours
end
return ibis