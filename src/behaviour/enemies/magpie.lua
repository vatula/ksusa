local Timer = require "vendor/hump.timer"
local Behaviours = require "entities.behaviours"
local Patrol = require "behaviour.patrol"
local TimedMotion = require "behaviour.motion_timed"
local FunctionT = require "behaviour.function_t"
local BehaviorGroup = require "behaviour.behaviorgroup"

local function switch(state)
    return function(entity)
        entity.signals:emit('stateChange', state, true)
    end
end

local function magpie()
    local behaviours = Behaviours()

    -- idle

    local idle1 = TimedMotion('y', 'linear', 1.2, 0)
    local idle2 = TimedMotion('y', 'linear', 1.2, 0)
    idle1:on('reset', function(entity)
        local rect = entity:getRect()
        entity.__initialPos = {x = rect.l, y = rect.t }
        if entity.__flips == nil then entity.__flips = 1 end
        entity:flip()
    end)
    idle1:on('finished', switch('idle2'))
    idle2:on('finished', function(entity)
        if entity.__flips > 0 then
            entity.__flips = entity.__flips - 1
            entity.signals:emit('stateChange', 'idle', true)
        else
            entity.__flips = nil
            entity.signals:emit('stateChange', 'patrol', true)
        end
    end)

    -- patrol

    local timedMotionY1 = TimedMotion('y', 'in-expo', 0.4, -6)

    local timedMotionY2 = TimedMotion('y', 'linear', 0.4, -10)

    local ellipseMotion = FunctionT(function(self, dt, entity)
        -- elliptic motion
        local rect = entity:getRect()
        local l, t = rect.l, rect.t
        entity.__initialTime = entity.__initialTime + dt
        l = entity.__patrolCircle.x + entity.__patrolCircle.d*100*math.cos(entity.__initialTime)
        t = entity.__patrolCircle.y + 40*math.sin(entity.__initialTime)
        local e = 70
        local diff = math.abs(entity.__initialPos.x - rect.l)

        entity.__instantSpeedX = math.abs((l - rect.l))/dt
        -- if approaching initial state, switch to retreat state
        if (diff <= e and entity.__patrolCircleCanPublish)
        then
            entity.__patrolCircleCanPublish = false
            entity.signals:emit('stateChange', 'retreat', true)
        elseif (diff > e and not entity.__patrolCircleCanPublish) then
            entity.__patrolCircleCanPublish = true
        end

        -- if dx changes, flip creature's direction
        if (l - rect.l < 0 and entity.direction > 0) or (l - rect.l > 0 and entity.direction < 0) then
            entity:flip()
        end

        entity:move(l, t, 0)
    end)
    ellipseMotion:on('reset', function(entity)
        entity.__initialTime = math.pi
        local rect = entity:getRect()
        local l, t = rect.l, rect.t
        entity.__patrolCircle = {
            d = entity.direction,
            x = l - entity.direction*100*math.cos(entity.__initialTime),
            y = t - 40*math.sin(entity.__initialTime)
        }
        entity.__patrolCircleCanPublish = false
    end)

    local timedMotionY3 = TimedMotion('y', 'linear', 0.4, -20)

    local patrol = Patrol(80, 100)
    timedMotionY1:on('finished', switch('patrol12'))

    timedMotionY2:on('finished', switch('patrol2'))

    timedMotionY3:on('finished', switch('patrol3'))

    patrol:on('enemyInSight', function(entity)
        entity.signals:emit('stateChange', 'attack', true)
    end)

    -- attack

    local attack = FunctionT(function(self, dt, entity)
        local current = entity:getRect()
        local finish = entity.__attackFinish
        local nextL = current.l + entity.direction*entity.__attackSpeed*dt
        local nextT = entity.__attackSlope*nextL + entity.__attackIntercept
        local e = 1
        local diff = math.abs(finish.x - nextL)
        entity:move(nextL, nextT, 0)
        if (diff <= e) then
            entity.__instantSpeedX = math.abs((current.l - nextL))/dt
            entity.signals:emit('stateChange', 'retreat', true)
        end
    end)
    attack:on('reset', function(entity)
        local current = entity:getRect()
        local endstate = { x = current.l + entity.direction*patrol.dx, y = current.t + patrol.dy }
        entity.__attackSpeed = -30
        Timer.add(0.2, function()
            entity.__attackSpeed = 80
        end)
        entity.__attackSlope = (endstate.y - current.t)/(endstate.x - current.l)
        entity.__attackIntercept = endstate.y - entity.__attackSlope*endstate.x
        entity.__attackFinish = endstate
    end)

    local retreatMotion = FunctionT(function(self, dt, entity)
        local current, finish = entity:getRect(), entity.__initialPos
        local nextT, nextL, diff
        if math.abs(entity.__retreatSlope) > 1 then
            nextT = current.t + entity.__retreatDirectionY*entity.__instantSpeedX*dt
            nextL = (nextT - entity.__retreatIntercept)/entity.__retreatSlope
            diff = math.abs(finish.y - nextT)
        else
            nextL = current.l + entity.direction*entity.__instantSpeedX*dt
            nextT = entity.__retreatSlope*nextL + entity.__retreatIntercept
            diff = math.abs(finish.x - nextL)
        end
        local e = 1
        if (diff <= e) then
            entity:move(finish.x, finish.y, 0)
            entity.signals:emit('stateChange', 'idle', true)
        else
            entity:move(nextL, nextT, 0)
        end
    end)
    retreatMotion:on('reset', function(entity)
        local current, endstate = entity:getRect(), entity.__initialPos
        local dy, dx = endstate.y - current.t, endstate.x - current.l
        entity.__retreatSlope = dy/dx
        entity.__retreatDirectionY = ((dy > 0) and 1) or -1
        entity.__retreatIntercept = endstate.y - entity.__retreatSlope*endstate.x
        if ((entity.direction < 0 and dx > 0) or (entity.direction > 0 and dx < 0)) then entity:flip() end
    end)

    -- TODO finish return for magpie

    behaviours:add('idle', BehaviorGroup(idle1))
    behaviours:add('idle2', BehaviorGroup(idle2))
    behaviours:add('patrol', BehaviorGroup(timedMotionY1))
    behaviours:add('patrol12', BehaviorGroup(timedMotionY2))
    behaviours:add('patrol2', BehaviorGroup(timedMotionY3))
    behaviours:add('patrol3', BehaviorGroup(ellipseMotion, patrol))
    behaviours:add('attack', BehaviorGroup(attack))
    behaviours:add('retreat', BehaviorGroup(retreatMotion))
    return behaviours
end
return magpie