local Timer = require "vendor/hump.timer"
local Behaviours = require "entities.behaviours"
local Patrol = require "behaviour.patrol"
local ContinuousMotion = require "behaviour.motion_continuous"
local Gravity = require "behaviour.gravity"
local BehaviorGroup = require "behaviour.behaviorgroup"

local function seagull()
    local behaviours = Behaviours()
    local idleGravity = Gravity(2)
    local gravity = Gravity(2)
    idleGravity:on('onGround', function(entity) entity.signals:emit('stateChange', 'patrol', true) end)

    local patrol = Patrol(100, 0)
    local attack = Patrol(100, 0)
    local patrolMotion = ContinuousMotion('x', 50)
    local attackMotion = ContinuousMotion('x', 100)
    local cancelAttackHandler

    patrol:on('enemyInSight', function(entity)
        if entity.__cancelAttackHandler then Timer.cancel(entity.__cancelAttackHandler) end
        entity.signals:emit('stateChange', 'attack', true)
    end)

    attack:on('enemyOutOfSight', function(entity)
        entity.__cancelAttackHandler = Timer.add(1, function()
            entity.signals:emit('stateChange', 'patrol', true)
        end)
    end)

    behaviours:add('idle', BehaviorGroup(idleGravity))
    behaviours:add('patrol', BehaviorGroup(patrol, patrolMotion, gravity))
    behaviours:add('attack', BehaviorGroup(attack, attackMotion, gravity))
    --behaviours:add('die', Behavior(DieCreature()))

    return behaviours
end

return seagull