local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Behaviour = require "behaviour/behaviour"

local MotionTimed = Class {
    __includes = { Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function MotionTimed:setup(axis, fn, duration, to)
    self.axis = axis
    self.timer = Timer.new()
    self.t = { value = 0 }
    self.last = 0
    self.status = 0 -- 0 = ready, 1 = finished, 2 = reported
    self.timer:tween(duration, self.t, {value = to}, fn, function() self.status = 1 end)
end

function MotionTimed:update(dt, entity)
    if self.status == 0 then
        local rect = entity:getRect()
        self.timer:update(dt)
        local l, t, delta = rect.l, rect.t, self.t.value - self.last
        if self.axis == 'y' then t = t + delta end
        if self.axis == 'x' then l = l + delta end
        entity:move(l, t, 0)
        self.last = self.t.value
    elseif self.status == 1 then
        self.status = 2
        self:emit('finished', entity, self.t.value)
    end
end

return MotionTimed