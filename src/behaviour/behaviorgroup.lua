local Class = require "vendor/hump.class"

local BehaviorGroup = Class {
    init = function(self, ...)
        self.behaviors = {}
        for _, behavior in ipairs({...}) do
            table.insert(self.behaviors, behavior)
        end
    end
}

function BehaviorGroup:update(dt, entity)
    for _, behavior in pairs(self.behaviors) do
        behavior:update(dt, entity)
    end
end

function BehaviorGroup:reset(entity)
    for _, behavior in pairs(self.behaviors) do
        behavior:reset(entity)
    end
end

return BehaviorGroup