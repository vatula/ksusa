local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"

local AttackRetreat = Class {
    init = function(self, entity, world)
        self.appearFor = 5
        self.waitFor = 2
        self.attackFor = 5
        self.retreatFor = 5
        self.dieFor = 5

        self.entity = entity
        self.world = world
        self.old_velocity = self.entity.velocity:clone()
        local behaviour = self
        self.handle = Timer.addPeriodic(self.appearFor + 2*self.waitFor + self.attackFor + self.retreatFor, function()
            Timer.do_for(behaviour.appearFor, function(dt) behaviour:appear(dt) end, function()
                Timer.do_for(behaviour.waitFor, function(dt) behaviour:wait(dt) end, function()
                    Timer.do_for(behaviour.attackFor, function(dt) behaviour:attack(dt) end, function()
                        Timer.do_for(behaviour.waitFor, function(dt) behaviour:wait(dt) end, function()
                            Timer.do_for(behaviour.retreatFor, function(dt) behaviour:retreat(dt) end)
                        end)
                    end)
                end)
            end)
        end)
    end;
    update = function(self, dt)
    end
}

function AttackRetreat:appear(dt)
    self.entity.left = self.entity.left - 50*dt
end

function AttackRetreat:wait(dt)

end

function AttackRetreat:attack(dt)
    self.entity.top = self.entity.top - 5*dt
end

function AttackRetreat:retreat(dt)
    self.entity.left = self.entity.left + 50*dt
    self.entity.top = self.entity.top + 5*dt
end

function AttackRetreat:die(dt)
end

function AttackRetreat:destroy()
    Timer.cancel(self.handle)
    Timer.do_for(self.dieFor, function(dt) self.die(dt) end)
end

--function WaitAttackRetreat:update(dt)
--end

return AttackRetreat