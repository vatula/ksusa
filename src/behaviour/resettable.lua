local Class = require "vendor/hump.class"

local function _pack(...) return {n=select('#', ...), ...} end
local function _unpack(t) return unpack(t, 1, t.n) end

local Resettable = Class {
    init = function(self, ...)
        self.__args = _pack(...)
    end
}

function Resettable:reset()
    self:setup(_unpack(self.__args))
end

function Resettable:setup(...) end

return Resettable