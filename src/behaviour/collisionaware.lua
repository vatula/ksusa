local Class = require "vendor/hump.class"

local CollisionAware = Class {
    init = function(self) end
}

function CollisionAware:move(entity, future_l, future_t, alpha, collisionFilter, tryResolveDelegate)
    local hardCols, hardLen = entity.world:check(entity, future_l, future_t, collisionFilter)
    if hardLen == 0 then
        entity:move(future_l, future_t, alpha)
    else -- collision with the ground
        local col, tl, tt, nx, ny, sl, st
        local visited = {}

        while hardLen > 0 do
            col = hardCols[1]

            if visited[col.other] then break end
            visited[col.other] = true

            local tl, tt, nx, ny, sl, st = col:getSlide()
            if tryResolveDelegate(tl, tt, nx, ny, sl, st, alpha, col) then break end
            hardCols, hardLen = entity.world:check(entity, sl, st, collisionFilter)
            if hardLen == 0 then
                entity:move(sl, st, alpha)
            end
        end
    end
end

return CollisionAware