local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"
local CollisionAware = require "behaviour/collisionaware"
local Behaviour = require "behaviour/behaviour"

local function collisionFilter(other)
    local tags = other.tags
    return tags ~= nil and tags['ground']
end

local Gravity = Class {
    __includes = { CollisionAware, Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function Gravity:setup(acceleration, velocity)
    self.gravity = acceleration
    self.velocity = velocity or 0
end

function Gravity:reset(entity)
    entity.__gravityVelocity = self.velocity
    Behaviour.reset(self, entity)
end

local function platformCollisionFilter(other)
    local tags = other.tags
    return tags ~= nil and tags['platform']
end

local function handlePlatformCollisions(entity, future_l, future_t)
    local platformCols, platformLen = entity.world:check(entity, future_l + entity.direction, future_t, platformCollisionFilter)

    if platformLen ~= 0 then
        local col = platformCols[1]
        local tl, tt, nx, ny = col:getTouch()
        -- will collide from the top
        if (ny == -1 and col.is_intersection == false) then
            col.other.tags['ground'] = true
        else
            col.other.tags['ground'] = false
        end
    end
end

function Gravity:update(dt, entity)
    local rect = entity:getRect()
    entity.__gravityVelocity = entity.__gravityVelocity + self.gravity*100*dt
    local future_l, future_t = rect.l, rect.t + entity.__gravityVelocity*dt
    handlePlatformCollisions(entity, future_l, future_t)
    local hitGround = false
    CollisionAware.move(self, entity, future_l, future_t, 0, collisionFilter, function(tl, tt, nx, ny, sl, st, alpha, col)
        entity.__gravityVelocity = 0
        hitGround = true
        entity:move(tl, tt, alpha)
        return false
    end)

    entity.__gravityOnGround = hitGround
    if hitGround then self:emit('onGround', entity) end
end

return Gravity