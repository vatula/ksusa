local Class = require "vendor/hump.class"
local Behaviour = require "behaviour/behaviour"

local function isPlayer(other)
    return (not not other.name) and other.name == 'Ksusa'
end

local function collisionFilter(other)
    local tags = other.tags
    return tags ~= nil and tags['ground']
end

local function sightFilter(other)
    local tags = other.tags
    return (tags ~= nil and tags['ground']) or isPlayer(other)
end

local Patrol = Class {
    __includes = { Behaviour },
    init = function(self, ...)
        Behaviour.init(self, ...)
    end
}

function Patrol:setup(dx, dy)
    self.dx = dx or 0
    self.dy = dy or 0
    self.enemyInSight = false
end

function Patrol:update(dt, entity)
    local rect = entity:getRect()
    local rangeX = rect.l + entity.direction*self.dx
    local rangeY = rect.t + self.dy
    local col, len = entity.world:check(entity, rangeX, rangeY, sightFilter)
    if (len > 0 and isPlayer(col[1].other) and not self.enemyInSight) then
        self.enemyInSight = true
        self:emit('enemyInSight', entity)
    elseif ((len == 0 or not isPlayer(col[1].other)) and self.enemyInSight) then
        self.enemyInSight = false
        self:emit('enemyOutOfSight', entity)
    end

end

return Patrol