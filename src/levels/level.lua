local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Status = require "entities.status"
local Death = require "entities.death"
local Signal = require "vendor/hump.signal"
local Level = Class {}

function Level:init(self)
    self.name = 'level'
end

-- Called when entering the state
function Level:enter(previous, background, map, player, cam)
    self.background = background
    self.map = map
    self.player = player
    self.death = nil
    self.cam = cam
    self.finished = false
    self.light = {alpha = 0}
    self.status = Status(self.player, self.cam)
    Signal.emit(self.name .. '.start')
end

-- Called when leaving a state.
function Level:leave()
end

function Level:update(dt)
    if not self.finished then
        if self.death then
            self.death:update(dt)
            dt = dt/6
        end
        self.map:update(dt)
        self.player:update(dt)

        local w = love.graphics.getWidth()/self.cam.scale
        local rect = self.player:getRect()
        local camLeft = math.max(math.min(rect.l, self.map.width-w/2), w/2)

        self.background:update(dt, camLeft - w/2, 300)
        self.cam:lookAt(camLeft, 200)

        if self.player.energy <= 0 and not self.death then
            self.death = Death(self.player, self.map.world)
        end
    end
end

function Level:finish(continuation)
    self.finished = true
    Timer.tween(0.6,
        self.light, {alpha = 255}, 'linear',
        continuation
    )
end

function Level:next()
    return self
end

function Level:draw()
    self.cam:draw(function ()
        self.background:draw()
        self.map:draw(function(layer)
            if layer.name == 'player' then
                self.player:draw()
            end
        end)
        if self.death then

            local r, g, b, a = love.graphics.getColor()
            local w, h = love.graphics.getWidth(), love.graphics.getHeight()

            love.graphics.setColor(0, 0, 0, self.death.opacity*0.6)
            love.graphics.rectangle('fill', 0, 0, w, h)
            love.graphics.setColor(r, g, b, a)

            self.death:draw()
        end

        if self.finished then
            local r, g, b, a = love.graphics.getColor()
            love.graphics.setColor(0, 0, 0, self.light.alpha)
            local x, y = self.cam:pos()
            local w = love.graphics.getWidth()/self.cam.scale
            local h = love.graphics.getHeight()/self.cam.scale

            love.graphics.rectangle("fill", x - w/2, y - h/2, w, h)

            love.graphics.setColor(r, g, b, a)
        end
    end)
    self.status:draw()
end

function Level:keypressed(key)
    if key == ' ' then
        Signal.emit('player.jump', self.player)
    elseif key == 'q' then
        Signal.emit('player.hit', self.player)
    end
end

-- Called on quitting the game. Only called on the active gamestate.
function Level:quit()
end

return Level