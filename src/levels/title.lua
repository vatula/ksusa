local vector = require "vendor/hump.vector"
local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Camera = require "vendor/hump.camera"
local anim8 = require "vendor/anim8/anim8"
local Signal = require "vendor/hump.signal"
local Title = Class {}

local function img(path)
    local img = love.graphics.newImage(path)
    img:setFilter('nearest', 'nearest')
    return img
end

local function background(image, wrap_x, wrap_y, ox, oy, motion, time)
    local pos = vector(ox, oy)
    image:setWrap(wrap_x, wrap_y)
    local quad = love.graphics.newQuad(0, 0, love.graphics.getWidth(), love.graphics.getHeight(), image:getWidth(), image:getHeight())

    local bck = {}
    Timer.do_for(time, function(dt) motion(dt, pos) end)

    function bck:update(dt)

    end
    function bck:draw()
--        love.graphics.draw(image)
        love.graphics.draw(image, quad, 0, 0, 0, 1, 1, pos.x, pos.y)
    end
    return bck
end

local function animation(ani, sprites, ox, oy, motion, time)
    local pos = vector(ox, oy)
    Timer.do_for(time, function(dt) motion(dt, pos) end)
    local a = {}
    function a:update(dt) ani:update(dt) end
    function a:draw() ani:draw(sprites, pos.x, pos.y) end
    return a
end

local function menu(image)
    local m = {}
    function m:update(dt)
    end
    function m:draw()
    end
    return m
end

function Title:init()
    local cam = Camera(love.graphics.getWidth()/2, 100)
    cam:zoomTo(2)
    self.cam = cam

    local w, h = love.graphics.getWidth()/self.cam.scale, love.graphics.getHeight()/self.cam.scale

    local bck = background(img('gfx/skyscrapers.png'), 'repeat', 'clamp', 110, 70, function(dt, pos)
        pos.y = pos.y + dt*5
    end, 6)
    local tree = background(img('gfx/title_tree.png'), 'clamp', 'clamp', -w/2, -h/2 - 90, function(dt, pos)
        pos.y = pos.y + dt*20
    end, 6)

    local ears_sprites = img('gfx/ears.png')
    local g = anim8.newGrid(201, 100, ears_sprites:getWidth(), ears_sprites:getHeight())
    local ears_ani = anim8.newAnimation(g('1-4', 1), {5, 0.1, 0.05, 0.1})
    local ears = animation(ears_ani, ears_sprites, w - 100, h + 55, function(dt, pos)
        pos.y = pos.y - dt*28
    end, 6)

    local menu = menu(img('gfx/title_tree.png'))
    local title = {}

    function title:update(dt)
        bck:update(dt)
        tree:update(dt)
        ears:update(dt)
        menu:update(dt)
    end

    function title:draw()
        bck:draw()
        tree:draw()
        ears:draw()
--        menu:draw()
    end

    self.title = title
    self.menu = menu
end

function Title:enter(previous)
end

function Title:keypressed(key)
    if key == 'return' then
        Signal.emit('game.start')
    end
end

function Title:draw()
    self.cam:draw(function()
        self.title:draw()
    end)
end

function Title:update(dt)
    self.title:update(dt)
end

return Title