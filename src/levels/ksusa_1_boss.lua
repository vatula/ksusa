local Camera = require "vendor/hump.camera"
local Class = require "vendor/hump.class"
local bump = require "vendor/bump/bump"
local get_background = require "configuration.backgrounds.backgrounds"
local animations = require "configuration.animations.animations"
local Player = require "entities.player"
local Map = require "entities.map"
local Level = require "levels.level"
local vector = require "vendor/hump.vector"

local Pantagruel = Class {
    __includes = Level
}

function Pantagruel:enter(previous)
    local world = bump.newWorld(50)
    local player = Player(world, {location = vector(10, 165), dimentions = vector(16, 28)}, animations.player)
    local map = Map(world, 'gfx/ksusa_1_boss')

    local cam = Camera(10, 100)
    cam:zoomTo(2)
    cam:lookAt(player.left + 16, 280)

    local background = get_background(player.left + 16, player.top + 16)

    Level.enter(self, previous, background, map, player, cam)
end

return Pantagruel