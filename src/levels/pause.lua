local Class = require "vendor/hump.class"
local Pause = Class {}

function Pause:init()
    self.font = love.graphics.newFont("gfx/pixcyr2.ttf", 48)
    self.font:setFilter('nearest', 'nearest')
end

function Pause:enter(from)
    self.from = from
end

function Pause:draw()
    local old_font = love.graphics.getFont()
    local r, g, b, a = love.graphics.getColor()
    local w, h = love.graphics.getWidth(), love.graphics.getHeight()
    -- draw previous screen
    self.from:draw()
    -- overlay with pause message
    love.graphics.setColor(0, 0, 0, 100)
    love.graphics.rectangle('fill', 0, 0, w, h)
    love.graphics.setColor(255, 255, 255)

    love.graphics.setFont(self.font)
    love.graphics.printf('ПАУЗА', 0, h/2, w, 'center')

    love.graphics.setFont(old_font)
    love.graphics.setColor(r, g, b, a)
end

return Pause