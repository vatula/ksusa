local Camera = require "vendor/hump.camera"
local Class = require "vendor/hump.class"
local bump = require "vendor/bump/bump"
local get_background = require "configuration.backgrounds.backgrounds"
local animations = require "configuration.animations.animations"
local Player = require "entities.player"
local Map = require "entities.map"
local Level = require "levels.level"
local vector = require "vendor/hump.vector"
local Behaviours = require "configuration.behaviours.behaviours"
local Animation = require "animation/animation"

local Level01 = Class {
    __includes = Level,
    init = function(self)
        self.name = 'level01'
    end
}

function Level01:enter(previous)
    local world = bump.newWorld(50)
    local behaviour = Behaviours:getBehaviours('player')
    local player = Player(world,
    {
        rect = {l = 150, t = 165, w = 16, h = 28}, energy = 100,
        animation = Animation(animations['player'].layers), behaviours = behaviour
    })
--    local player = Player(world, {location = vector(150, 165), dimentions = vector(16, 28)}, animations.player)
    local map = Map(world, 'gfx/ksusa_1')

    local cam = Camera(320, 100)
    local rect = player:getRect()
    cam:zoomTo(2)
    cam:lookAt(rect.l + 16, 280)

    local background = get_background(map.width, map.height, rect.l + 16, rect.t + 16)

    Level.enter(self, previous, background, map, player, cam)
end

return Level01