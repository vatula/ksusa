local Class = require "vendor/hump.class"
local Camera = require "vendor/hump.camera"
local Timer = require "vendor/hump.timer"

local LevelCleared = {}

function LevelCleared:init()
    self.font = love.graphics.newFont("gfx/glimstick.ttf", 22)
    self.font:setFilter('nearest', 'nearest')
    self.cam = Camera(love.window.getWidth()/2, love.window.getHeight()/2)
    self.cam:zoomTo(2)
end

local function shake(self, scale)
    return function()
        local orig_x, orig_y = self.cam:pos()
        Timer.do_for(2*scale,
            function(dt) self.cam:lookAt(orig_x + math.random(-1,1), orig_y + math.random(-1,1)) end,
            function()
                Timer.do_for(1*scale, function(dt)
                    self.cam:lookAt(orig_x + math.random(-1,0), orig_y + math.random(-1,0))
                end, function()
                    self.cam:lookAt(orig_x, orig_y)
                end)
            end)
    end
end

function LevelCleared:enter(previous)
    shake(self, 1)()
    Timer.addPeriodic(5, shake(self, 0.2))
end

function LevelCleared:update(dt)
end

function LevelCleared:draw()
    local r, g, b, a = love.graphics.getColor()
    local old_font = love.graphics.getFont()

    local cam_x, cam_y = self.cam:pos()
    self.cam:draw(function()
        love.graphics.setColor(0, 0, 0, 255)
        love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setFont(self.font)
        love.graphics.printf("Потрясающая\nКсюша", 0, love.window.getHeight()/3, love.window.getWidth(), 'center')

        love.graphics.setFont(old_font)
        love.graphics.setColor(r, g, b, a)
    end)
end

return LevelCleared