local level01 = require "levels.level_01"
local title = require "levels.title"
local levelCleared = require "levels.level_cleared"
local boss01 = require "levels.ksusa_1_boss"
local pause = require "levels.pause"
local Gamestate = require "vendor/hump.gamestate"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"
local Soundbank = require "entities/soundbank"

local function restart()
    Gamestate.switch(Gamestate.current())
end

local function start()
    Gamestate.switch(level01)
end

function love.load()
    love.window.setMode(640, 480, {fullscreen=false, vsync=false, resizable=false})
    love.graphics.setBackgroundColor(111, 189, 237, 0)
    Signal.register('player.dead', restart)
    Signal.register('game.start', start)
    Soundbank({})
    --[[Soundbank({
        { on = 'player.attack', path = 'sfx/pattack.wav', loop = false },
        { on = 'item.consumed', path = 'sfx/consumable.wav', loop = false},
        { on = 'player.jump', path = 'sfx/pjump.wav', loop = false },
        { on = 'creature.damaged', path = 'sfx/phit.wav', loop = false },
        { on = 'level01.start', path = 'sfx/anxiety.ogg', loop = true },
    })]]--
    Gamestate.registerEvents()
    Gamestate.switch(title)
end

function love.update(dt)
    Timer.update(dt)
    local state = Gamestate.current()
    if state.player and state.player.consumed['exit'] > 0 then
        state.player.consumed['exit'] = 0
        state:finish(function()
            Gamestate.switch(state:next())
        end)
    end
end

function love.keypressed(key)
    if Gamestate.current() ~= pause and key == 'p' then
        Gamestate.push(pause)
    elseif Gamestate.current() == pause and key == 'p' then
        Gamestate.pop()
    end
end