local Background = require "entities.background"

local function build_quad(w, h, path)
    local img = love.graphics.newImage(path)
    img:setFilter('nearest', 'nearest')
    img:setWrap('repeat', 'repeat')
    local quad = love.graphics.newQuad(0, 0, w, h, img:getWidth(), img:getHeight())
    local bck = { img = img, quad = quad }

    return bck
end

local function get_background(w, h, offsetX, offsetY)
    local background_0 = build_quad(w, h, 'gfx/clouds_day.png')
    local background_1 = build_quad(w, h, 'gfx/hills.png')
    local background_2 = build_quad(w, h, 'gfx/skyscrapers2.png')
    local background_3 = build_quad(w, h, 'gfx/hills2.png')

    local background = Background(offsetX, offsetY)
    background:attachLayer(background_0, -1.0, 0, 300, 2, 0)
--    background:attachLayer(background_1, -1.0, 0, 360, 0, 0)
--    background:attachLayer(background_2, -0.97, 0, 360, 0, 0)
--    background:attachLayer(background_3, -0.95, 0, 335, 0, 0)

    return background
end

return get_background