local playerBehaviour = require "behaviour/player/player"
local seagullBehavior = require "behaviour/enemies/seagull"
local magpieBehavior = require "behaviour/enemies/magpie"
local ibisBehavior = require "behaviour/enemies/ibis"

-- factory
local all = {
    player = playerBehaviour,
    seagull = seagullBehavior,
    magpie = magpieBehavior,
    ibis = ibisBehavior
}

local BehavioursConfig = {}

function BehavioursConfig:getBehaviours(name)
    return all[name]()
end

return BehavioursConfig