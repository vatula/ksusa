local consumables = {}
consumables.chocolate = {
    layers = {
        default = {
            width = 16, height = 16,
            states = {
                idle = {
                    sprites = 'gfx/consumables_1.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}
consumables.nutella = {
    layers = {
        default = {
            width = 16, height = 16,
            states = {
                idle = {
                    sprites = 'gfx/consumables_1.png',
                    columns = 1,
                    rows = 2,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}
return consumables