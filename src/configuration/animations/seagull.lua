local seagull = {
    layers = {
        skin = {
            width = 16, height = 16,
            states = {
                idle = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = '1-6',
                    rows = 1,
                    delays = 0.15
                },
                walk = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = '1-6',
                    rows = 1,
                    delays = 0.15
                },
                attack = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = '1-6',
                    rows = 2,
                    delays = 0.1
                },
                die = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = '1-6',
                    rows = 1,
                    delays = 0.15,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return seagull