local player = {
    layers = {
        a_character = {
            width = 32, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/char.png',
                    columns = '1-8',
                    rows = 1,
                    delays = {2.0, 0.1, 1.0, 0.1, 1.0, 0.1, 0.1, 0.1}
                },
                walk = {
                    sprites = 'gfx/char.png',
                    columns = '1-6',
                    rows = 7,
                    delays = {0.15, 0.05, 0.25, 0.15, 0.05, 0.25}
                },
                jump = {
                    sprites = 'gfx/char.png',
                    columns = 1,
                    rows = 2,
                    delays = {0.1},
                    onLoop = 'pauseAtEnd'
                },
                jumpattack = {
                    sprites = 'gfx/char.png',
                    columns = '1-4',
                    rows = 2,
                    delays = {0.1, 0.1, 0.1, 0.1},
                    onLoop = 'pauseAtStart'
                },
                attack = {
                    sprites = 'gfx/char.png',
                    columns = '1-3',
                    rows = 4,
                    delays = {0.1, 0.1, 0.1},
                    onLoop = 'pauseAtStart'
                }
            }
        },
        --[[ab_hair = {
            width = 32, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/char_1_idle_hair.png',
                    columns = '1-8',
                    rows = 1,
                    delays = {2.0, 0.1, 1.0, 0.1, 1.0, 0.1, 0.1, 0.1}
                }
            }
        },--]]
        --[[b_pajama = {
            width = 32, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/char_1_tot.png',
                    columns = '1-8',
                    rows = 1,
                    delays = {2.0, 0.1, 1.0, 0.1, 1.0, 0.1, 0.1, 0.1}
                },
                walk = {
                    sprites = 'gfx/char_2_walk_tot.png',
                    columns = '1-4',
                    rows = 1,
                    delays = 0.3
                },
                jump = {
                    sprites = 'gfx/char_3_jump_tot.png',
                    columns = 1,
                    rows = 1,
                    delays = {0.15},
                    onLoop = 'pauseAtEnd'
                }
            }
        },--]]
        z_attack = {
            active = false,
            width = 48, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/char_4_hit.png',
                    columns = '1-3',
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                walk = {
                    sprites = 'gfx/char_4_hit.png',
                    columns = '1-3',
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                jump = {
                    sprites = 'gfx/char_4_hit.png',
                    columns = '1-3',
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return player