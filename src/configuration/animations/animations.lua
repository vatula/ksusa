local player = require "configuration.animations.player"
local enemy = require "configuration.animations.enemy"
local seagull = require "configuration.animations.seagull"
local magpie = require "configuration.animations.magpie"
local ibis = require "configuration.animations.ibis"
local pantagruel = require "configuration.animations.pantagruel"
local consumables = require "configuration.animations.consumables"
local exit = require "configuration.animations.exit"

local animations = {
    player = player,
    enemy = enemy,
    seagull = seagull,
    magpie = magpie,
    ibis = ibis,
    pantagruel = pantagruel,
    exit = exit,
    consumables = consumables
}

return animations