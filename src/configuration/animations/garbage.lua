local garbage = {
    layers = {
        skin = {
            width = 32, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/items.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.15,
                    onLoop = 'pauseAtEnd'
                },
                fall = {
                    sprites = 'gfx/items.png',
                    columns = '1-3',
                    rows = 1,
                    delays = {0.4, 0.15, 0.15},
                    onLoop = 'pauseAtEnd'
                },
                explode = {
                    sprites = 'gfx/items.png',
                    columns = '4-8',
                    rows = 1,
                    delays = 0.06,
                    onLoop = 'pauseAtEnd'
                },
                die = {
                    sprites = 'gfx/items.png',
                    columns = 8,
                    rows = 1,
                    delays = 1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return garbage