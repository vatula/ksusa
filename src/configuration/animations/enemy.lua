local enemy = {
    layers = {
        skin = {
            width = 16, height = 16,
            states = {
                idle = {
                    sprites = 'gfx/test_enemy.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.1
                },
                walk = {
                    sprites = 'gfx/test_enemy.png',
                    columns = '1-5',
                    rows = 1,
                    delays = 0.1
                },
                die = {
                    sprites = 'gfx/test_enemy.png',
                    columns = '1-5',
                    rows = 2,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return enemy