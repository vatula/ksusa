local exit = {
    layers = {
        skin = {
            width = 16, height = 16,
            states = {
                idle = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                consumed = {
                    sprites = 'gfx/enemy.seagull.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return exit