local death = {
    layers = {
        death = {
            width = 64, height = 78,
            states = {
                idle = {
                    sprites = 'gfx/death.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.3,
                    onLoop = 'pauseAtEnd'
                },
                away = {
                    sprites = 'gfx/death.png',
                    columns = '2-12',
                    rows = 1,
                    delays = {0.1, 0.1, 0.1, 0.1, 0.1, 0.5, 0.1, 0.5, 0.1, 0.1, 0.1},
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}
return death