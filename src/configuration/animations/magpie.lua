local magpie = {
    layers = {
        skin = {
            width = 24, height = 32,
            states = {
                idle = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '1-8',
                    rows = 3,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                idle2 = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '8-1',
                    rows = 3,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                patrol = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '1-4',
                    rows = 5,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                patrol12 = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = 4,
                    rows = 5,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                patrol2 = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '6-8',
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                patrol3 = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '1-8',
                    rows = 1,
                    delays = 0.1
                },
                attack = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '2-4',
                    rows = 2,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                retreat = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '1-8',
                    rows = 1,
                    delays = 0.1
                },
                die = {
                    sprites = 'gfx/enemy.magpie.png',
                    columns = '1-5',
                    rows = 6,
                    delays = 0.06,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}

return magpie