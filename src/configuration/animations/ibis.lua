local ibis = {
    layers = {
        skin = {
            width = 64, height = 64,
            states = {
                idle = {
                    sprites = 'gfx/enemy.ibis.png',
                    columns = '1-4',
                    rows = 4,
                    delays = 0.15
                },
                flap1 = {
                    sprites = 'gfx/enemy.ibis.png',
                    columns = '1-4',
                    rows = 2,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                flap2 = {
                    sprites = 'gfx/enemy.ibis.png',
                    columns = '1-6',
                    rows = 1,
                    delays = 0.1,
                    onLoop = 'pauseAtEnd'
                },
                patrol = {
                    sprites = 'gfx/enemy.ibis.png',
                    columns = '1-6',
                    rows = 1,
                    delays = 0.1
                }
            }
        }
    }
}

return ibis