local pantagruel = {
    layers = {
        face = {
            width = 175, height = 200,
            states = {
                idle = {
                    sprites = 'gfx/babyface.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.15,
                    onLoop = 'pauseAtEnd'
                },
                butt = {
                    sprites = 'gfx/babyface02.png',
                    columns = 1,
                    rows = 1,
                    delays = 0.15,
                    onLoop = 'pauseAtEnd'
                }
            }
        }
    }
}
return pantagruel