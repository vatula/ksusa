
--camera.lua
--Basic camera functions


Class = require "../vendor/hump.class"

ParallaxCamera = Class {
    init = function(self)
        self.x = 0
        self.y = 0

        self.xMax = 0
        self.yMax = 0

        self.scaleX = 1
        self.scaleY = 1
        self.rotation = 0
        self.shakeTime = 1

        self.speed = 800

        self.dx = 0
        self.dy = 0

        self.platformWidth = 0
        self.platformHeight = 0

        self.zoomFactor = 1
        self.imageModifier = 1
        self.windowWidth = self.imageModifier * love.graphics.getWidth()
        self.windowHeight = love.graphics.getHeight()

        self.canvas = {}
        self.canvas[1] = {} -- "ceiling"
        self.canvas[2] = {} -- "floor"
    end
}

--canvas system:
function ParallaxCamera:draw(pX, pY)

    self:set()

    local j = 1


    while j <= #self.canvas[1] do --size: 2

        for i = 0, 2, 1 do --number of columns

            local screenPortionX = self.x/self.windowWidth --portion of the screen in a max of 800 (screenWidth)
            local screenPortionY = self.y/self.windowHeight --portion of the screen in a max of 600 (screenHeight)


            for k = -1, 0, 1 do --number of rows

                local step = math.floor(1 * screenPortionY) --always 0 ?
                local scrollIndex = step + k + 2
                local screenRate = self.canvas[scrollIndex][j].scrollSpeed

                -- for 2 rows only:     --current image at current (floored) screen position:
                if scrollIndex <= 2 and self.canvas[scrollIndex][j].media[math.floor(screenRate * screenPortionX) + i] then

                    love.graphics.draw( self.canvas[scrollIndex][j].media[math.floor(screenRate * screenPortionX) + i],
                        pX + ((i-1)*self.windowWidth - math.fmod(screenRate * (self.x), self.windowWidth)),
                        (self.windowHeight * k) + (pY + ((math.fmod(screenRate * self.y, self.windowHeight)) + self.canvas[scrollIndex][j].yOffset)),
                        0,
                        self.canvas[scrollIndex][j].scale,self.canvas[scrollIndex][j].scale, 0, 0)
                end

            end
        end
        j = j + 1
    end

    self:unset()

end

--camera update functions (pan right, left, up, down):

function ParallaxCamera:update(dt)
    if love.keyboard.isDown("right") then
        self:moveRight(dt)
    end

    if love.keyboard.isDown("left") then
        self:moveLeft(dt)
    end

    if love.keyboard.isDown("up") then
        self:moveUp(dt)
    end

    if love.keyboard.isDown("down") then
        self:moveDown(dt)
    end
end

--Camera starting offset:
function ParallaxCamera:offset(cX, cY)
    self.x = cX
    self.y = cY
end

--Camera set max pan width:
function ParallaxCamera:setPlatform(width)
    self.platformWidth = width
    self.platformHeight = self.windowHeight

    self.xMax = self.platformWidth - self.windowWidth
    self.yMax = self.windowHeight

    print("platWidth " .. (self.platformWidth))
    print("platHeight " .. (self.platformHeight))
    print("xMax " .. (self.xMax))
    print("yMax " .. (self.yMax))

end

--New camera layer function:
function ParallaxCamera:newLayer(layerCode, layerOrder, screenLimit, scrollSpeed, imageScale, yOff)

    backgroundLayers = {}

    for i =1, screenLimit, 1 do
        local imageName = layerCode..i
        local buffer = love.image.newImageData("map/"..imageName..".png")
        backgroundLayers[i] = love.graphics.newImage(buffer)
    end;

    -- populate the backgroundLayers table, inserting media and scrollspeed
    table.insert(self.canvas[layerOrder], {media = backgroundLayers, scrollSpeed = scrollSpeed, scale = imageScale, yOffset = yOff})
    --sort by ascending scrollspeed
    table.sort(self.canvas[layerOrder], function(a, b) return a.scrollSpeed < b.scrollSpeed end)

    --unload current image
    buffer = nil

end

--Camera move Right:
function ParallaxCamera:moveRight(dt)

    if self.x >= 0 and self.x < self.xMax then
        self.x = self.x + (self.speed * dt)
    else
        self.x = self.xMax
    end

end

--Camera move Left:
function ParallaxCamera:moveLeft(dt)

    if self.x > 0 and self.x < self.platformWidth then
        self.x = self.x - (self.speed * dt)
        if self.x < 0 then
            self.x = 0
        end
    end

end

--Camera move Up:
function ParallaxCamera:moveUp(dt)

    local boundary = 5
    if self.y >= 0 and self.y < (self.yMax - boundary) then
        self.y = self.y + (self.speed * dt)
        if self.y > (self.yMax - boundary) then
            self.y = self.yMax - boundary
        end
    end

end

--Camera move Down:
function ParallaxCamera:moveDown(dt)

    if self.y > 0 then
        self.y = self.y - (self.speed * dt)
        if self.y < 0 then
            self.y = 0
        end
    end

end

--Camera set transformations:
function ParallaxCamera:set()
    love.graphics.push()
    love.graphics.scale(self.zoomFactor * self.scaleX, self.zoomFactor * self.scaleY)
    love.graphics.translate(self.dx, self.dy)
end

--Camera unset transformations:
function ParallaxCamera:unset()
    love.graphics.pop()
end

--Camera Scale:
function ParallaxCamera:scale(sx, sy)
    sx = sx or 1
    self.scaleX = self.scaleX * sx
    self.scaleY = self.scaleY * (sy or sx)
end

--Camera set Position:
function ParallaxCamera:setPosition(x, y)
    self.x = x or self.x
    self.y = y or self.y
end

--Camera set Scale:
function ParallaxCamera:setScale(sx, sy)
    self.scaleX = 1/sx or self.scaleX
    self.scaleY = 1/sy or self.scaleY
end

--Camera set Translation
function ParallaxCamera:setTranslation(tx,ty)
    self.dx = tx
    self.dy = ty
end

--Camera get-position functions:
function ParallaxCamera:getX()
    return self.x
end

function ParallaxCamera:getY()
    return self.y
end
