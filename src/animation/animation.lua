local Layer = require "animation.layer"
local Class = require "vendor/hump.class"
local utils = require "utils.utils"

local Animation = Class {
    init = function(self, configuration)
        self.flip = false
        self.layers = {}
        self.state = ''

        for name, layerCfg in pairs(configuration) do
            self.layers[name] = Layer(name, layerCfg)
        end
    end
}

function Animation:update(dt)
    local stateAnimation
    for _,layer in pairs(self.layers) do
        local active = layer.config.active
        if active == nil or active then
            stateAnimation = layer:getState(self.state)
            if (self.flip and not stateAnimation.flipped) or (not self.flip and stateAnimation.flipped) then
                stateAnimation:flip()
            end
            stateAnimation:update(dt)
        end
    end
end

function Animation:draw(rect)
    local l, t, w, h = rect.l, rect.t, rect.w, rect.h
    local x, y
    for _,layer in utils:spairs(self.layers) do
        local active = layer.config.active
        if active == nil or active then
--          Draw layers in the middle of the body
            x, y = l + (w - layer.config.width)/2, t + (h - layer.config.height)/2
            layer:getState(self.state):draw(x, y)
        end
    end
end

function Animation:setState(state)
    local oldState = self.state
    if oldState ~= state then
        self:rewind(oldState)
        self.state = state
    end
end

function Animation:rewind(state)
    for _,layer in pairs(self.layers) do
        layer:getState(state):rewind()
    end
end

function Animation:flipHorizontal()
    self.flip = not self.flip;
end

return Animation
