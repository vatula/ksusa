local LayerState = require "animation.layerState"
local Class = require "vendor/hump.class"
local anim8 = require "vendor/anim8/anim8"

local Layer = Class {
    init = function(self, name, config)
        local sprites, x, y, delays, onLoop, img, g

        self.config = config
        self.currentImage = nil
        self.name = name
        self.states = {}
        local cache = {}
        for state, stateCfg in pairs(config.states) do
            sprites, x, y, delays, onLoop = stateCfg.sprites, stateCfg.columns, stateCfg.rows, stateCfg.delays, stateCfg.onLoop
            img = cache[sprites]
            if img == nil then
                img = love.graphics.newImage(sprites)
                img:setFilter('nearest', 'nearest')
                cache[sprites] = img
            end
            g = anim8.newGrid(self.config.width, self.config.height, img:getWidth(), img:getHeight())
            self.states[state] = LayerState(img, anim8.newAnimation(g(x, y), delays, onLoop))
        end
        self:setState('idle')
    end
}

function Layer:setState(state)
    local s = self.states[state]
    if s ~= nil then
        self.currentState = s
    end
end

function Layer:getState(state)
    return self.states[state] or self.currentState
end

return Layer