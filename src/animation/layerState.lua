local Class = require "vendor/hump.class"

local LayerState = Class {
    init = function(self, img, anim)
        self.img, self.anim, self.flipped = img, anim, false
    end
}

function LayerState:draw(x, y)
    self.anim:draw(self.img, x, y)
end

function LayerState:update(dt)
    self.anim:update(dt)
end

function LayerState:flip()
    self.flipped = not self.flipped
    self.anim:flipH()
end

function LayerState:rewind()
    self.anim:gotoFrame(1)
    self.anim:resume()
end

return LayerState