local Class = require "vendor/hump.class"
local vector = require "vendor/hump.vector"
local Timer = require "vendor/hump.timer"
local Animated = require "entities.animated"
local animation = require "configuration/animations/death"
local _ = require "vendor/underscore/underscore"
local Signal = require "vendor/hump.signal"

local Death = Class {
    __includes = Animated,
    init = function(self, player, world)
        Animated.init(self, world, {location = vector(self.left, self.top), dimentions = vector(64, 78)}, animation, 'idle')
        self.player = player
        self.duration = animation.layers.death.states.away.delays
        self.takeAway = false
        self.left = 0
        self.top = 0
        self.opacity = 0

        Timer.add(0.5, function()
            local x, y = self.player.left + self.player.width/2 - self.width/2, self.player.top + self.player.height - 78
            self.left = x
            self.top = y - 15

            local copy = _.reverse(self.duration)
            local wait = _.reduce(_.rest(copy), 0, function(memo, i) return memo + i end)

            Timer.tween(0.1, self, {top = y, opacity = 255}, 'linear', function()
                Timer.add(0.5, function()
                    self.state = 'away'
                    Timer.add(wait, function()
                        local d = _.pop(copy)
                        Timer.tween(d, self, {top = self.top - 15, opacity = 0}, 'linear')
                        Timer.tween(d, self.player, { top = self.player.top - 15, opacity = 0}, 'linear', function()
                            Signal.emit('player.dead')
                        end)
                    end)
                end)
            end)
        end)
    end
}

function Death:draw()
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(r, g, b, self.opacity)
    Animated.draw(self)
    love.graphics.setColor(r, g, b, a)
end

function Death:update(dt)
    Animated.update(self, dt)
end

return Death