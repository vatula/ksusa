local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"

local Status = Class {
    init = function(self, player, camera)
        self.player = player
        self.scale = camera.scale
        self.life = love.graphics.newImage("gfx/life.png")
        self.life:setFilter('nearest', 'nearest')
    end
}

function Status:update(dt)
end

function Status:draw()
    local r, g, b, a = love.graphics.getColor()
    local w, h = love.graphics.getWidth(), love.graphics.getHeight()

    local life = self.player.energy / 100
    local from = 1 + 10*(1 - life)
    love.graphics.draw(self.life, love.graphics.newQuad(0, 0, 11, 10, 22, 10), 5, 10, 0, self.scale, self.scale)
    love.graphics.draw(self.life, love.graphics.newQuad(11, from, 11, 10, 22, 10), 5, 10+from*self.scale, 0, self.scale, self.scale)

    love.graphics.setColor(r, g, b, a)
end


return Status