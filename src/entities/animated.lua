local Class = require "vendor/hump.class"
local Animation = require "animation/animation"

local Animated = Class {
    init = function(self, world, bodyConfig, animationConfig, state)
        self.world, self.state, self.left, self.top, self.width, self.height =
        world, state, bodyConfig.location.x, bodyConfig.location.y,
        bodyConfig.dimentions.x, bodyConfig.dimentions.y

        self.world:add(self, self.left, self.top, self.width, self.height)
        self.animation = Animation(animationConfig.layers)
        self.debug = false
    end
}

function Animated:update(dt)
    self.animation:setState(self.state)
    self.animation:update(dt)
end

function Animated:draw()
    self.animation:draw({l = self.left, t = self.top, w = self.width, h = self.height})
    if self.debug then
        love.graphics.setColor(255,255,255)
        love.graphics.setLineWidth(1)
        love.graphics.rectangle("line", self.left, self.top, self.width, self.height)
    end
end

function Animated:destroyPhysics()
    self.world:remove(self)
end

return Animated