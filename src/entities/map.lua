local sti = require "vendor/sti"
local animations = require "configuration.animations.animations"
local Creature = require "entities.creature.creature"
local Pantagruel = require "entities.pantagruel"
local Consumable = require "entities.consumable"
local Class = require "vendor/hump.class"
local vector = require "vendor/hump.vector"
local Behaviours = require "configuration.behaviours.behaviours"
local Animation = require "animation/animation"
local Timer = require "vendor/hump.timer"

local enemyTypes = {
    [325] = 'seagull',
    [326] = 'magpie',
    [327] = 'ibis'
}

local function removeEnemy(enemies, i)
    return function(enemy)
        Timer.add(1, function()
            enemies[i] = nil
        end)
    end
end

local function registerEnemy(enemiesTable, enemy)
    enemy.tags = {['enemy'] = true }
    table.insert(enemiesTable, enemy)
    enemy.signals:register('destroyed', removeEnemy(enemiesTable, #enemiesTable))
end

local function addEnemy(enemies, i)
    return function(parent, child)
        registerEnemy(enemies, child)
    end
end

-- on collide with ground, set pos to -8 from the top of the tile
local function process(map, world)
    local enemiesTable = {}

    local ground = { ['ground'] = true }
    local blocker = { ['blocker'] = true }
    local platform = { ['platform'] = true }

    local groundLayer = map:getCollisionMap('ground')
    local enemyBlockerLayer = map:getCollisionMap('enemy blocker')
    local platformLayer = map:getCollisionMap('platform')
    local enemiesLayer = map:getCollisionMap('enemies')
    local platformTopOffset = map.layers['platform']['properties']['offset']
    local w, h = map.tilewidth, map.tileheight
    for x=1, map.width do
        for y=1, map.height do
            local groundCollision = groundLayer.data[y][x]
            local platformCollision = platformLayer.data[y][x]
            local enemyCollision = enemiesLayer.data[y][x]
            local enemyBlocker = enemyBlockerLayer.data[y][x]
            if groundCollision == 1 then
--                Body of a ground must be with an offset since it is kinda isometric
                local ctile = { tags = ground }
                world:add(ctile, (x - 1)*w, (y - 1)*h + 8, w, h)
            end
            if enemyBlocker == 1 then
                local ctile = { tags = blocker }
                world:add(ctile, (x - 1)*w, (y - 1)*h, w, h)
            end
            if platformCollision == 1 then
                local ctile = { tags = platform }
                world:add(ctile, (x - 1)*w, (y - 1)*h + platformTopOffset, w, h - platformTopOffset)
            end
            if enemyCollision == 1 then
                local enemyType = enemyTypes[map.layers['enemies'].data[y][x].gid]
                local behaviour = Behaviours:getBehaviours(enemyType)
                local enemy = Creature(world,
                    {
                        rect = {l = (x - 1)*w, t = (y - 1)*h, w = 16, h = 16}, energy = 100,
                        animation = Animation(animations[enemyType].layers), behaviours = behaviour
                    })
                registerEnemy(enemiesTable, enemy)
                enemy.signals:register('attached', addEnemy(enemiesTable, #enemiesTable))
            end
        end
    end
    world:add({ tags = ground }, -w, 0, w, map.height*h)
    world:add({ tags = ground }, map.width*w, 0, w, map.height*h)
    return enemiesTable
end

local function updateEnemies(dt, enemies)
    for _, enemy in pairs(enemies) do
        enemy:update(dt)
    end
end
local function drawEnemies(enemies)
    for _, enemy in pairs(enemies) do
        enemy:draw()
    end
end

local function processConsumables(map, world)
    local exit, consumables = {}, {}
    local consumablesLayer = map.layers['consumables']

    for _, object in ipairs(consumablesLayer.objects) do
        local x, y = consumablesLayer.x + object.x, consumablesLayer.y + object.y
        if object.type == 'portal' and object.name == 'exit' then
            exit = Consumable(object.name, world, {location = vector(x, y), dimentions = vector(16, 32)}, animations.exit)
            exit.tags['exit'] = true
        else
            table.insert(consumables, Consumable(object.name, world, {
                location = vector(x, y),
                dimentions = vector(16, 16)},
                animations.consumables[object.name]))
        end
    end

    function consumablesLayer:update(dt) end
    function consumablesLayer:draw() end

    return exit, consumables
end

local empty = {}
function empty:update(dt) end
function empty:draw(dt) end

local Map = Class {
    init = function(self, world, tiledMapPath)
        self.world = world
        self.data = sti.new(tiledMapPath)
        self.width = self.data.width*self.data.tilewidth
        self.height = self.data.height*self.data.tileheight
        self.enemies = process(self.data, self.world)
        self.exit, self.consumables = processConsumables(self.data, self.world)
        self.exit = self.exit or empty

    end
}

function Map:update(dt)
    self.data:update(dt)
    self.exit:update(dt)
    for k,v in ipairs(self.consumables) do v:update(dt) end
    updateEnemies(dt, self.enemies)
end

function Map:draw(onDraw)
    self.data:draw(nil, nil, function(layer)
        if layer.name == 'enemies' then
            drawEnemies(self.enemies)
        end
        onDraw(layer)
    end)

    self.exit:draw()

    for k,v in ipairs(self.consumables) do v:draw() end
end

return Map