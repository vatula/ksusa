local Class = require "vendor/hump.class"
local Background = Class {
    init = function(self, offsetX, offsetY)
        self.x = offsetX
        self.y = offsetY
        self.layers = {}
    end
}

function Background:attachLayer(layer, distance, x, y, vx, vy)
    table.insert(self.layers, {layer = layer, distance = distance, x = x, y = y, vx = vx, vy = vy})
    table.sort(self.layers, function(a, b) return a.distance < b.distance end)
end

function Background:update(dt, x, y)
    for _, v in ipairs(self.layers) do
        v.x = v.x + dt * v.vx
        v.y = v.y + dt * v.vy
        v.lx = (x * v.distance) + v.x
        v.ly = (y * v.distance) + v.y
    end
end

function Background:draw()
    for _, v in ipairs(self.layers) do
        love.graphics.draw(v.layer.img, v.layer.quad, 0, 0, 0, 1, 1, v.lx, v.ly)
    end
end

return Background