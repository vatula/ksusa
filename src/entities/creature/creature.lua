local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"

local Creature = Class {
    init = function(self, world, config)
        self.direction = -1 -- facing left
        self.signals = Signal.new()
        self.animation = config.animation
        self.world = world
        self.world:add(self, config.rect.l, config.rect.t, config.rect.w, config.rect.h)
        self.__cache = {rect = self.world:getRect(self)}
        self.behavior = nil
        self.hitMask = {value = 255 }
        self.energy = config.energy

        self.signals:register('stateChange', function(state, reset)
            local oldState = self.animation.state
            reset = reset or false
            self.animation:setState(state)
            self.behavior = config.behaviours:getByState(state)
            if reset then self.behavior:reset(self) end
        end)

        self.signals:register('damaged', function(value) self:acceptDamage(value) end)
        self.signals:emit('stateChange', 'idle', true)
    end
}

function Creature:attach(name, item)
    item.__parent = self
    self.signals:emit('attached', self, item)
end

function Creature:acceptDamage(value)
    self.energy = self.energy - value
    if self.energy <= 0 then
        self:die()
        self:destroy()
    else
        self:damage()
    end
end

function Creature:canUpdateBehavior()
    return self.hitMask.value == 255
end

function Creature:update(dt)
    if self:canUpdateBehavior() then
        self.behavior:update(dt, self)
    end

    self.animation:update(dt)
end

function Creature:move(left, top, alpha)
    self.world:move(self, left, top)
    self.__cache['rect'] = self.world:getRect(self)
end

function Creature:flip()
    self.direction = -self.direction
    self.animation:flipHorizontal()
end

function Creature:getRect()
    return self.__cache['rect']
end

function Creature:draw()
    local r, g, b, a = love.graphics.getColor()
    local mode = love.graphics.getBlendMode()

    local rect = self:getRect()
    self.animation:draw(rect)
    --    if creature is hit, draw hit overlay
    if self.hitMask.value ~= 255 then
        love.graphics.setBlendMode("additive")
        love.graphics.setColor(255, 255, 255, self.hitMask.value)
        self.animation:draw(rect)
    end

    love.graphics.setBlendMode(mode)
    love.graphics.setColor(r, g, b, a)
end

function Creature:destroy()
    self.world:remove(self)
    self.signals:emit('destroyed', self)
    self.signals:clear_pattern('.*')
    Signal.emit('creature.destroyed', self)
end

function Creature:damage()
    Timer.tween(0.3, self.hitMask, { value = 0 }, 'expo', function() self.hitMask.value = 255 end)
    Signal.emit('creature.damaged')
end

function Creature:die()
    self.signals:emit('stateChange', 'die')
end

return Creature