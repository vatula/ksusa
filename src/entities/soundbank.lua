local Class = require "vendor/hump.class"
local Signal = require "vendor/hump.signal"

local function play(sound)
    return function() sound:play() end
end

local Soundbank = Class {
    init = function(self, sound_conf)
        local handles = {}
        for _, conf in pairs(sound_conf) do
            local source = love.audio.newSource(conf.path, "static")
            source:setLooping(conf.loop)
            local action = play(source)
            handles[conf.on] = action
            Signal.register(conf.on, action)
        end
    end
}

return Soundbank