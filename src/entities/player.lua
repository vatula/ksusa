local Class = require "vendor/hump.class"
local Creature = require "entities/creature/creature"
local Signal = require "vendor/hump.signal"
local Timer = require "vendor/hump.timer"
local Collider = require "behaviour.collider"

local function meleeLocation(player)
    local rect = player:getRect()
    local left = rect.l + rect.w/2 + ((player.direction == -1 and -24) or 0)
    return { l = left, t = rect.t, w = 24, h = 32 }
end

local Melee = Class {
    init = function(self, parent, world, damage, active)
        self.parent = parent
        self.damage = damage
        self.active = active
        self.world = world
        local location = meleeLocation(parent)
        self.world:add(self, location.l, location.t, location.w, location.h)
        self.collider = Collider(function(melee, other)
            local tags = other.tags
            return tags ~= nil and tags['enemy'] and not melee.parent.__invincible and melee.active
        end, function(tl, tt, nx, ny, sl, st, alpha, col)
            if col.other.hitMask.value == 255 then
                col.other.signals:emit('damaged', 50)
            end
        end)
    end
}

function Melee:update(dt)
    if self.active then
        self.collider:update(dt, self)
    end
end

function Melee:move(l, t, alpha)
    local alpha = alpha or 0
    self.world:move(self, l, t)
end

local Player = Class {
    __includes = { Creature },
    init = function(self, world, config)
        Creature.init(self, world, config)
        self.name = 'Ksusa'
        self.direction = 1 -- facing right
        self.melee = Melee(self, world, 50, false)
    end,
    consumed = { exit = 0 }
}

function Player:canUpdateBehavior()
    return self.hitMask.value == 255 and not self.melee.active
end

function Player:update(dt)
    self.melee:update(dt)
    Creature.update(self, dt)
end

function Player:damage()
    Timer.tween(0.3, self.hitMask, { value = 0 }, 'expo', function() self.hitMask.value = 255 end)
    Signal.emit('player.damaged')
end

function Player:hit()
    local attackLayer = self.animation.layers['z_attack']
    local attackConfig = attackLayer.config
    if not attackConfig.active then
        Signal.emit('player.attack')
        attackConfig.active = true
        local current_state = self.animation.state
        local attack_state = 'attack'
        if current_state == 'jump' then
            attack_state = 'jumpattack'
        end
        self.animation:setState(attack_state)
        self.melee.active = true
        Timer.add(0.3, function()
            self.melee.active = false
            attackConfig.active = false
            attackLayer:getState(self.animation.state):rewind()
            self.animation:setState(current_state)
        end)
    end

end

function Player:move(left, top, alpha)
    Creature.move(self, left, top, alpha)
    local location = meleeLocation(self)
    self.melee:move(location.l, location.t, alpha)
end

return Player