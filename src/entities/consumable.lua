local Animated = require "entities.animated"
local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"

local Consumable = Class {
    __includes = Animated,
    init = function(self, name, world, bodyConfig, animationConfig)
        Animated.init(self, world, bodyConfig, animationConfig, 'idle')
        self.name = name
        self.left = bodyConfig.location.x
        self.top = bodyConfig.location.y
        self.consumed = false
        self.tags = {['consumable'] = true }
        self.glow = 0
        self.opacity = 255
        self.hGlowIn = {}
        self.hGlowOut = {}

        local glowin, glowout
        glowin = function() self.hGlowIn = Timer.tween(0.6, self, { glow = 128 }, 'linear', glowout) end
        glowout = function() self.hGlowOut = Timer.tween(0.6, self, { glow = 0 }, 'linear', glowin) end
        glowin()
    end
}

function Consumable:update(dt)
    if self.consumed then
        Signal.emit('item.consumed')
        Timer.cancel(self.hGlowIn)
        Timer.cancel(self.hGlowOut)
        self.glow = 0
        local top = self.top - 20
        Timer.tween(0.3, self, {top = top, opacity = 0 }, 'out-expo')
        self.state = 'consumed'
        Animated.destroyPhysics(self)
        self.consumed = false
    end
    Animated.update(self, dt)
end

function Consumable:draw()
    local r, g, b, a = love.graphics.getColor()
    local mode = love.graphics.getBlendMode()
    love.graphics.setColor(r, g, b, self.opacity)
    Animated.draw(self)
    -- glow
    love.graphics.setBlendMode("additive")
    love.graphics.setColor(255, 255, 255, self.glow)
    Animated.draw(self)
    love.graphics.setBlendMode(mode)
    love.graphics.setColor(r, g, b, a)
end

return Consumable