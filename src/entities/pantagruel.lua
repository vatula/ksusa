--[[local WaitAttackRetreat = require "behaviour.attack_retreat"
local Class = require "vendor/hump.class"
local Creature = require "entities.creature"

local Pantagruel = Class {
    __includes = Creature,
    init = function(self, world, bodyConfig, config)
        Creature.init(self, world, bodyConfig, config, 'idle')
        self.tags = {['boss'] = true }
        self.behaviour = WaitAttackRetreat(self, world)
    end,
    direction = 'left'
}

function Pantagruel:update(dt)
    self.behaviour:update(dt)
    Creature.update(self, dt)
end

return Pantagruel]]--