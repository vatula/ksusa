local Class = require "vendor/hump.class"
local Timer = require "vendor/hump.timer"
local Signal = require "vendor/hump.signal"
local Behaviour = require "behaviour/behaviour"

local empty = Class {
    __includes = { Behaviour },
    init = function(self)
        Behaviour.init(self)
    end;
    update = function(self) end;
    draw = function(self) end
}

local Behaviours = Class {
    init = function(self)
        self.all = {}
    end
}

function Behaviours:add(name, behaviour)
    self.all[name] = behaviour
end

function Behaviours:getByState(state)
    return self.all[state] or empty
end

function Behaviours:empty()
    return empty
end

return Behaviours